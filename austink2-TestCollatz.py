#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 1)
        self.assertEqual(v, 179)

    def test_eval_6(self):
        v = collatz_eval(1, 999999)
        self.assertEqual(v, 525)

    def test_eval_7(self):
        v = collatz_eval(333, 1)
        self.assertEqual(v, 144)

    def test_eval_8(self):
        v = collatz_eval(800000, 999999)
        self.assertEqual(v, 525)
        
    def test_eval_9(self):
        v = collatz_eval(999999, 999100)
        self.assertEqual(v, 396)
        
    def test_eval_10(self):
        v = collatz_eval(1, 12000)
        self.assertEqual(v, 268)
        
    def test_eval_11(self):
        v = collatz_eval(16, 10000)
        self.assertEqual(v, 262)
        
    def test_eval_12(self):
        v = collatz_eval(70000, 200000)
        self.assertEqual(v, 383)
        
    def test_eval_13(self):
        v = collatz_eval(56799, 150000)
        self.assertEqual(v, 375)
    
    def test_eval_14(self):
        v = collatz_eval(900000, 999999)
        self.assertEqual(v, 507)
        
    def test_eval_15(self):
        v = collatz_eval(25, 25000)
        self.assertEqual(v, 282)
        
    def test_eval_16(self):
        v = collatz_eval(1, 100000)
        self.assertEqual(v, 351)
        
    def test_eval_17(self):
        v = collatz_eval(90000, 1)
        self.assertEqual(v, 351)
        
    def test_eval_18(self):
        v = collatz_eval(1, 8050)
        self.assertEqual(v, 262)
        
    def test_eval_19(self):
        v = collatz_eval(2500, 25000)
        self.assertEqual(v, 282)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n1000 1\n1 999999\n333 1\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n1000 1 179\n1 999999 525\n333 1 144\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


$ coverage report -m                   >> TestCollatz.out



$ cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
